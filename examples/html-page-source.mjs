import { parser, lexer } from "./index"

let source = `
<html>
  <body>
    {% for let item of this.items %}
      \${item.description}
      {% if item.name == "mine" %}
        It's mine!
      {% else if item.name == "" %}
        It's no ones.
      {% else %}
        It's \${item.name}
      {% end %}
    {% end %}
  </body>
</html>
`
let tokens = Array.from(lexer(source)),
    ast = parser(tokens, "main")

/* eslint-disable no-console */
console.dir(tokens, { depth: null })
console.dir(ast, { depth: null })
console.log(ast.toString())
console.log(source)
