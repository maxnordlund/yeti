/* eslint-disable */
"use strict"

class Template {
  toString(locals) {
    Object.assign(this, locals)
    return Array.from(this).join("")
  }
}

class MainTemplate extends Template {
      *[Symbol.iterator]() {
        yield `
<html>
  <body>
    `;for (let item of this.items) {yield `
      ${item.description}
      `;if (item.name == "mine") {yield `
        It's mine!
      `;}else if (item.name == "") {yield `
        It's no ones.
      `;}else {yield `
        It's ${item.name}
      `;}yield `
    `;}yield `
  </body>
</html>`;
      }
    }

let template = new MainTemplate()
console.log(template.toString({
  items: [{
    name: "",
    description: "Round and blue"
  }, {
    name: "Erik",
    description: "Square and red"
  }, {
    name: "mine",
    description: "Pentagon and green"
  }]
}))
