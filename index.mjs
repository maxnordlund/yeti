export class Template {
  toString(locals) {
    Object.assign(this, locals)
    return Array.from(this).join("")
  }
}

const KEYWORD_REGEXP = /^(\S+)\s*(\S+)?/

export function parser(source, name) {
  let root = new RootNode(name),
      current = root

  if (typeof source == "string") {
    source = lexer(source)
  }

  for (let token of source) {
    switch (token.type) {
      case "text": {
        current.body.push(new TextNode(current, token))
        break
      }
      case "tag": {
        let expression = token.source.trim()
        if (expression === "") {
          let error = new SyntaxError("Empty tag")
          error.token = token
          throw error
        }

        let [match, keyword, modifier] = expression.match(KEYWORD_REGEXP)
        switch (keyword) {
          case "if":
          case "for":
          case "while": {
            current = new TernaryTagNode(current, token, keyword, expression.slice(keyword.length))
            current.parent.body.push(current)
            break
          }
          case "else": {
            current = current.parent
            if (modifier === "if") {
              current = new TernaryTagNode(current, token, "else if", expression.slice(match.length))
            } else {
              current = new TagNode(current, token, keyword)
            }
            current.parent.body.push(current)
            break
          }
          case "end": {
            current = current.parent
            break
          }
          default: {
            current = new TagNode(current, token, keyword)
            current.parent.body.push(current)
            break
          }
        }
        break
      }
    }
  }

  return root
}

export function* lexer(source) {
  let endColumn = 0,
      endIndex = 0,
      endLine = 1,
      startColumn = 1,
      startIndex = 0,
      startLine = 1

  // let lines = source.split("\n")

  for (let i = 0; i < source.length; ++i) {
    let character = source[i]

    ++endColumn
    endIndex = i

    switch (character) {
      case "\r": {
        if (source[i+1] === "\n") {
          ++i // Handle Windows line endings
        }
      }
      // fall through
      case "\n": {
        endColumn = 0
        ++endLine
        break
      }
      case "%": {
        if (source[i-1] === "{") {
          // Start of statement, end of literal text
          // Example: text...{%
          yield _token("text", { ei: -1, ec: -2 })
        } else if (source[i+1] === "}") {
          // End of statement, start of literal text
          // Example: %}...text
          ++i
          ++endColumn
          yield _token("tag", { sc: -2 })
        } else if (source[i+1] === "%") {
          // Escaped procent sign, `%%`, ignore the first...
          yield _token("text")
        } else if (source[i-1] === "%") {
          // ...and output the second, thus unescaping it.
          yield _token("text", { ei: 1 })
        } else {
          let error = new SyntaxError("Missing one of %{}")
          error.token = _token("error")
          throw error
        }

        // Either case advance to the next token
        startColumn = endColumn+1
        startIndex = i+1
        startLine = endLine
        break
      }
    }
  }

  // Don't forget any trailing text
  if (startLine != endLine || startColumn != endColumn+1) {
    yield _token("text")
  }

  // s -> start, e -> end
  // i -> index, c -> column
  function _token(name, delta={ si: 0, sc: 0, ei: 0, ec: 0 }) {
    delta.si |= 0
    delta.sc |= 0
    delta.ei |= 0
    delta.ec |= 0

    return {
      type: name,
      source: source.slice(startIndex + delta.si, endIndex + delta.ei),
      loc: {
        start: {
          index: startIndex + delta.si,
          line: startLine,
          column: startColumn + delta.sc
        },
        end: {
          index: endIndex + delta.ei,
          line: endLine,
          column: endColumn + delta.ec
        }
      }
    }
  }
}

export class Node {
  constructor(parent) {
    this.parent = parent
  }
}

export class RootNode extends Node {
  constructor(name) {
    super(null)
    this.name = name.slice(0, 1).toUpperCase()+name.slice(1)
    this.body = []
  }

  toString() {
    return `class ${this.name}Template extends Template {
      *[Symbol.iterator]() {
        ${this.body.join("")}
      }
    }`
  }
}

export class TextNode extends Node {
  constructor(parent, token) {
    super(parent)
    this.token = token
  }

  toString() {
    return `yield \`${this.token.source}\`;`
  }
}

export class TagNode extends Node {
  constructor(parent, token, keyword) {
    super(parent)
    this.token = token
    this.keyword = keyword
    this.body = []
  }

  toString() {
    return `${this.keyword} {${this.body.join("")}}`
  }
}

export class TernaryTagNode extends TagNode {
  constructor(parent, token, keyword, test) {
    super(parent)
    this.token = token
    this.keyword = keyword
    this.test = test
    this.body = []
  }

  toString() {
    return `${this.keyword} (${this.test.trim()}) {${this.body.join("")}}`
  }
}
