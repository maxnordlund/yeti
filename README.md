# Yeti
A small, simple, super fast templating engine. It compiles to plain ES6 classes
that implement the [iterable protocol][1]. This means it `yields` chunks of
output, aka streaming rendering.

[1]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols

Input:

```jinja
<html>
  <body>
    {% for let item of this.items %}
      ${item.description}
      {% if item.name == "mine" %}
        It's mine!
      {% else if item.name == "" %}
        It's no ones.
      {% else %}
        It's ${item.name}
      {% end %}
    {% end %}
  </body>
</html>
```

Output, formatted for readability:

```javascript
class MainTemplate extends Template {
  *[Symbol.iterator]() {
    yield `
<html>
  <body>
    `;

    for (let item of this.items) {
      yield `
      ${item.description}
      `;

      if (item.name == "mine") {
        yield `
        It's mine!
      `;
      } else if (item.name == "") {
        yield `
        It's no ones.
      `;
      } else {
        yield `
        It's ${item.name}
      `;
      }

      yield `
    `;
    }

    yield `
  </body>
</html>`;
  }
}
```
